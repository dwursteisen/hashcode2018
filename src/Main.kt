import java.io.File

fun main(args: Array<String>) {


    solve("a_example.in", "a.out", true)
    solve("b_should_be_easy.in", "b.out", true)
    solve("c_no_hurry.in", "c.out", true)
    solve("d_metropolis.in", "d.out", true)
    solve("e_high_bonus.in", "e.out", true)
}

data class Meta(val R: Int, val C: Int, val F: Int, val N: Int, val B: Int, val T: Int)
data class Ride(val id: Int, val a: Int, val b: Int, val x: Int, val y: Int, val s: Int, val f: Int)

data class Car(var x: Int, var y: Int, var rides: List<Ride> = emptyList(), var currentTurn: Int = 0)

fun solve(input: String, output: String, prioDistance: Boolean = false) {

    println("START $input -> $output")

    println("reading file")
    val inputFile = File(input)
    val lines = inputFile.readLines()


    /*
    The first line of the input file contains the following integer numbers separated by single spaces:
● R – number of rows of the grid (1 ≤ R ≤ 10000)
● C – number of columns of the grid (1 ≤ C ≤ 10000)
● F – number of vehicles in the fleet (1 ≤ F ≤ 1000)
● N – number of rides (1 ≤ N ≤ 10000)
● B – per-ride bonus for starting the ride on time (1 ≤ B ≤ 10000)
● T – number of steps in the simulation (1 ≤ T ≤ 10 )
     */


    val firstLine = lines.first().split(" ")
    val R = firstLine[0]
    val C = firstLine[1]
    val F = firstLine[2]
    val N = firstLine[3]
    val B = firstLine[4]
    val T = firstLine[5]

    val meta = Meta(R.toInt(), C.toInt(), F.toInt(), N.toInt(), B.toInt(), T.toInt())

    println("READ META $meta")


    println("READ $N LINES")

    val other = lines.drop(1)

    /*
    N subsequent lines of the input file describe the individual rides, from ride 0 to ride N − 1 . Each line
    contains the following integer numbers separated by single spaces:
    ● a – the row of the start intersection (0 ≤ a < R)
    ● b – the column of the start intersection (0 ≤ b < C)
    ● x – the row of the finish intersection (0 ≤ x < R)
    ● y – the column of the finish intersection (0 ≤ y < C)
    ● s – the earliest start(0 ≤ s < T)
    ● f – the latest finish (0 ≤ f ≤ T) , (f ≥ s + |x − a| + |y − b|)
    ○ note that f can be equal to T – this makes the latest finish equal to the end of the simulation
    */


    val rides = other.mapIndexed { id, l ->
        val split = l.split(" ")
        val a = split[0].toInt()
        val b = split[1].toInt()
        val x = split[2].toInt()
        val y = split[3].toInt()
        val s = split[4].toInt()
        val f = split[5].toInt()
        Ride(id, a, b, x, y, s, f)
    }

    assert(rides.size == N.toInt())
    println("FINISHED TO READ $N rides")

    val cars = compute(meta, rides, prioDistance)


    /*
    File format
The submission file must contain F lines, one for each vehicle in the fleet.
Each line describing the rides of a vehicle must contain the following integers separated by single spaces:
● M – number of rides assigned to the vehicle (0 ≤ M ≤ N)
● R0
, R1
, ..., RM-1 – ride numbers assigned to the vehicle, in the order in which the vehicle will perform
them (0 ≤ R )
i < N
Any ride can be assigned to a vehicle at most once. That is, it is not allowed to assign the same ride to
two or more different vehicles. It is also not allowed to assign the same ride to one vehicle more than once.
It is not required to assign all rides to vehicles – some rides can be skipped
     */

    val outputFile = File(output)

    val o = cars.map {
        val r = it.rides.map { it.id }.joinToString(" ")
        "${it.rides.size} $r"
    }.joinToString("\n")

    outputFile.writeText(o)


}


fun compute(meta: Meta, r: List<Ride>, prioBonus: Boolean): List<Car> {
    val rides = r
    println("STARTING COMPUTATION")


    /*
    tri par ordre d'arriver croissant.
    prendre la 1er voiture on prend le premier


     */

    println("SORT")

    var sorted = if (prioBonus) {

        val aaa = compareByDescending<Ride> { Math.abs(it.a - it.x) + Math.abs(it.b - it.y) }
        rides.sortedWith(aaa)


    } else {
        rides.sortedBy { it.s }
    }

    println("create cars")
    var cars = emptyList<Car>()
    for (i in 0..meta.F) {
        val c = Car(0, 0)
        cars += c
    }

    println("assign cars")

    var search = true
    while (search) {


        val avantBoucle = sorted
        for (c in cars) {
            fun accept(departVoiture: Pair<Int, Int>, depart: Pair<Int, Int>, arrive: Pair<Int, Int>, tourActuel: Int, departExpected: Int, tourExpected: Int): Int {
                val d1 = Math.abs(departVoiture.first - depart.first) + Math.abs(departVoiture.second - depart.second)
                val d2 = Math.abs(depart.first - arrive.first) + Math.abs(depart.second - arrive.second)

                val d = d1 + d2

                val attenteAuDebut = tourActuel + d1 >= departExpected
                val rideValide = tourActuel + d <= tourExpected

                return if (attenteAuDebut && rideValide) {
                    tourActuel + d
                } else {
                    -1
                }
            }

            fun accept2(departVoiture: Pair<Int, Int>, depart: Pair<Int, Int>, arrive: Pair<Int, Int>, tourActuel: Int, departExpected: Int, tourExpected: Int): Int {
                val d1 = Math.abs(departVoiture.first - depart.first) + Math.abs(departVoiture.second - depart.second)
                val d2 = Math.abs(depart.first - arrive.first) + Math.abs(depart.second - arrive.second)

                val d = d1 + d2


                val tempsAttente = departExpected - (tourActuel + d1)
                val rideValide = tourActuel + d + Math.max(0, tempsAttente) <= tourExpected

                return if (rideValide) {
                    tourActuel + d
                } else {
                    -1
                }
            }

            var tourSimulation = c.currentTurn


            var validR = sorted.map {
                accept(
                        departVoiture = c.x to c.y,
                        depart = it.a to it.b,
                        arrive = it.x to it.y,
                        tourActuel = tourSimulation,
                        departExpected = it.s,
                        tourExpected = it.f


                ) to it
            }.firstOrNull { it.first >= 0 }


            if (validR == null) {
                validR = sorted.map {
                    accept2(
                            departVoiture = c.x to c.y,
                            depart = it.a to it.b,
                            arrive = it.x to it.y,
                            tourActuel = tourSimulation,
                            departExpected = it.s,
                            tourExpected = it.f


                    ) to it
                }.firstOrNull { it.first >= 0 }
            }

            if (validR == null) {
                continue
            } else {
                tourSimulation += validR.first

                c.currentTurn = tourSimulation

                c.rides += validR.second
                sorted -= validR.second
            }

        }

        if(sorted == avantBoucle) {
            search = false
        }
    }

    println("END COMPUTATION")

    return cars
}